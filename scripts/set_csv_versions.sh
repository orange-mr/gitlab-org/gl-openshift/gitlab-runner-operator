#!/bin/bash

source scripts/_common.sh

CURRENT_RELEASE="$OPERATOR_VERSION"
PREVIOUS_RELEASE=""
if "$CERTIFIED"; then
  # previous release can not be RC
  # 1. because RedHat doesn't like to replace rc - certification tests fail
  # 2. because it only makes sense for a release to replace a stable release
  RELEASES=$(git tag -l | grep -v rc | ./hack/scripts/sort_semver.py)
  CURRENT_RELEASE=$(git tag -l | ./hack/scripts/sort_semver.py | head -n 1)
  if [[ $(echo "$RELEASES" | wc -l) -gt 1 ]]; then
    PREVIOUS_RELEASE=$(echo "$RELEASES" | head -n 2 | tail -n 1)
  fi
fi

if ! command -v yq &> /dev/null
then
    echo "yq is not installed. Download v4 from https://github.com/mikefarah/yq"
    exit
fi

IMAGE=$(yq e .metadata.name bundle/manifests/gitlab-runner-operator.clusterserviceversion.yaml | cut -f1 -d".")
yq e ".metadata.name = \"$IMAGE.$CURRENT_RELEASE\"" -i bundle/manifests/gitlab-runner-operator.clusterserviceversion.yaml
if [[ -z "$PREVIOUS_RELEASE" ]]; then
  yq e "del(.spec.replaces)" -i bundle/manifests/gitlab-runner-operator.clusterserviceversion.yaml
else
  yq e ".spec.replaces = \"$IMAGE.$PREVIOUS_RELEASE\"" -i bundle/manifests/gitlab-runner-operator.clusterserviceversion.yaml
fi

echo "CSV .metadata.name set to $(yq e .metadata.name bundle/manifests/gitlab-runner-operator.clusterserviceversion.yaml)"
echo "CSV .spec.replaces set to $(yq e .spec.replaces bundle/manifests/gitlab-runner-operator.clusterserviceversion.yaml)"