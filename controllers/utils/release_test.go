package utils

import (
	"fmt"
	"os"
	"testing"
)

func TestGetReleaseDir(t *testing.T) {
	dir, err := getReleaseDir()
	if err != nil {
		fmt.Printf("error getting releases directory; %v\n", err)
	}

	if dir == "" {
		t.Error("the release directory not found")
	}

	_, err = os.Stat(dir)
	if os.IsExist(err) {
		t.Error("invalid release directory")
	}
}
