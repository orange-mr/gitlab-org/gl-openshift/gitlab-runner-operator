package runner

import (
	gitlabutils "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/utils"
)

const (
	// RunnerImage has name of runner
	RunnerImage string = "gitlab-runner"

	// HelperImage has name of runner helper
	HelperImage string = "gitlab-runner-helper"
)

func getImageURL(image string) string {
	release := gitlabutils.GetRelease()

	var targetContainer gitlabutils.Image
	for _, container := range release.Images {
		if container.Name == image {
			targetContainer = container
		}
	}

	return targetContainer.Image()
}

func getHelperImage() string {
	return getImageURL(HelperImage)
}

func getRunnerImage() string {
	return getImageURL(RunnerImage)
}
