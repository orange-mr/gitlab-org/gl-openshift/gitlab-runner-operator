module gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator

go 1.13

require (
	github.com/go-logr/logr v0.1.0
	github.com/imdario/mergo v0.3.9
	k8s.io/api v0.18.6
	k8s.io/apimachinery v0.18.6
	k8s.io/client-go v0.18.6
	sigs.k8s.io/controller-runtime v0.6.3
	sigs.k8s.io/yaml v1.2.0
)
