[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator "Go Report Card")](https://goreportcard.com/report/gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator)

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# GitLab Runner Operator

The GitLab Runner operator aims to manage the lifecycle of GitLab Runner instances in your Kubernetes or Openshift container platforms.

While new and still actively being developed, the operator aims to:

- ease installation and configuration of GitLab and GitLab Runner instances
- offer seamless upgrades from version to version
- ease backup and restore of GitLab and its components
- aggregate and visualize metrics using Prometheus and Grafana
- setup auto-scaling

## Requirements
The GitLab Runner operator uses native Kubernetes resources to deploy and manage GitLab Runner instances in your cluster. It therefore will presumably run on any container platform that is derived from Kubernetes.

## Design Decisions

Decisions made during the design of the operator have been compiled into a
[document](doc/design-decisions.md) with background information to provide
reasoning for reaching the decision.

## Owned Resources

The operator is responsible for owns, watches and reconciles three different primary resources at this time.

#### 1. GitLab Runner
GitLab Runner is the open source project used to run your jobs and send the results back to GitLab. It is used in conjunction with GitLab CI/CD, the open-source continuous integration service included with GitLab that coordinates jobs.

An example is shown below:

```
apiVersion: apps.gitlab.com/v1beta2
kind: Runner
metadata:
  name: example
spec:
  gitlab:
    url: https://gitlab.com
  token: runner-token-secret # Name of the secret containing the Runner token
  tags: openshift, test
```


## OpenShift Cluster Setup

See [doc/openshift-cluster-setup.md](doc/openshift-cluster-setup.md) for instructions on how to create an OpenShift cluster for development or CI.

## More Information

More information on the operator can be found in the [Operator Documentation](https://gitlab.com/gitlab-org/gl-openshift/documentation) repo.

## Support and Maintenance

The GitLab Runner operator is supported and [maintained](https://about.gitlab.com/handbook/engineering/development/ops/verify/runner/#projects-we-maintain) by the GitLab Runner team. 

