/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta2

import (
	"errors"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var runnerlog = logf.Log.WithName("runner-resource")

// SetupWebhookWithManager adds  the runner webhook to the controller runtime manager
func (r *Runner) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
// +kubebuilder:webhook:verbs=create;update,path=/validate-apps-gitlab-com-v1beta2-runner,mutating=false,failurePolicy=fail,groups=apps.gitlab.com,resources=runners,versions=v1beta2,name=vrunner.kb.io

var _ webhook.Validator = &Runner{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *Runner) ValidateCreate() error {
	runnerlog.Info("validate create", "name", r.Name)

	if r.Spec.GitLab == "" {
		return errors.New("spec.gitlabUrl is empty")
	}

	if r.Spec.RegistrationToken == "" {
		return errors.New("spec.token is empty")
	}

	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *Runner) ValidateUpdate(old runtime.Object) error {
	runnerlog.Info("validate update", "name", r.Name)

	if r.Spec.GitLab == "" {
		return errors.New("spec.gitlabUrl is empty")
	}

	if r.Spec.RegistrationToken == "" {
		return errors.New("spec.token is empty")
	}

	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *Runner) ValidateDelete() error {
	runnerlog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}
