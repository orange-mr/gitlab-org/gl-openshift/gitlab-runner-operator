# Build the manager binary
FROM golang:1.13 as builder

WORKDIR /workspace
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy the go source
COPY main.go main.go
COPY api/ api/
COPY controllers/ controllers/

# Introduce the build arg check in the end of the build stage
# to avoid messing with cached layers
ARG VERSION

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -ldflags "-X main.buildVersion=${VERSION} -X main.buildDate=`date -u +%Y-%m-%d`" -o manager main.go

RUN test -n "$VERSION" || (echo "VERSION not set" && false)

# Use Red Hat UBI base image
FROM registry.access.redhat.com/ubi8/ubi-minimal:latest

ARG VERSION

LABEL name=gitlab-runner-operator \
      vendor='GitLab, Inc.' \
      version=$VERSION \
      release=$VERSION \
      description='Operator to manage GitLab Runner instances' \
      summary='GitLab Runner runs your CI jobs and sends results back to GitLab'

ENV USER_ID=1001

# Add license
COPY LICENSE /licenses/GITLAB

# Copy templates and releases
COPY hack/assets /

WORKDIR /
COPY --from=builder /workspace/manager .
USER ${USER_ID}

ENTRYPOINT ["/manager"]
