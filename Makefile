# Current Operator version
VERSION ?= 0.0.1
# Even if an empty VERSION is passed we override it
# since we always need VERSION. This is useful for dev builds
# of the operator and bundle images
ifeq ($(VERSION),)
override VERSION := 0.0.1
endif

# The Revision is used for unofficial builds since we need a semver-compatible
# version for the bundle
ifneq ($(REVISION),)
override VERSION := $(VERSION)-$(REVISION)
endif

# Current GitLab Runner version
APP_VERSION ?= v13.9.0

# Toggles between building certified images for RedHat's registry
# or building for the GitLab registry
CERTIFIED ?= true

# Default bundle image tag
BUNDLE_IMG ?= registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator-bundle:v$(VERSION)
GITLAB_CHANGELOG_VERSION ?= master
GITLAB_CHANGELOG = .tmp/gitlab-changelog-$(GITLAB_CHANGELOG_VERSION)
# Options for 'bundle-build'
BUNDLE_CHANNELS ?= stable
BUNDLE_DEFAULT_CHANNEL ?= stable
BUNDLE_METADATA_OPTS ?= --channels=$(BUNDLE_CHANNELS) --default-channel=$(BUNDLE_DEFAULT_CHANNEL)

# Image URL to use all building/pushing image targets
IMG ?= registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator:latest
# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= "crd:trivialVersions=true,preserveUnknownFields=false"

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

all: manager

# Run tests
ENVTEST_ASSETS_DIR = $(shell pwd)/testbin
test: generate fmt vet manifests
	mkdir -p $(ENVTEST_ASSETS_DIR)
	test -f $(ENVTEST_ASSETS_DIR)/setup-envtest.sh || curl -sSLo $(ENVTEST_ASSETS_DIR)/setup-envtest.sh https://raw.githubusercontent.com/kubernetes-sigs/controller-runtime/v0.6.3/hack/setup-envtest.sh
	source $(ENVTEST_ASSETS_DIR)/setup-envtest.sh; fetch_envtest_tools $(ENVTEST_ASSETS_DIR); setup_envtest_env $(ENVTEST_ASSETS_DIR); go test ./... -coverprofile cover.out

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet manifests
	go run ./main.go

# Install CRDs into a cluster
install: manifests kustomize
	$(KUSTOMIZE) build config/crd | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall: manifests kustomize
	$(KUSTOMIZE) build config/crd | kubectl delete -f -

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests kustomize
	cd config/manager && $(KUSTOMIZE) edit set image controller=${IMG}
	$(KUSTOMIZE) build config/default | kubectl apply -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests: controller-gen
	$(CONTROLLER_GEN) $(CRD_OPTIONS) rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd/bases

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Generate code
generate: controller-gen
	$(CONTROLLER_GEN) object:headerFile="hack/boilerplate.go.txt" paths="./..."

# Build the docker image
docker-build: test
	docker build -t gitlab-runner-operator:v${VERSION} --build-arg VERSION=v${VERSION} .

docker-tag:
	docker tag gitlab-runner-operator:v${VERSION} ${PROJECT}/gitlab-runner-operator:v${VERSION}

# Push the docker image
docker-push:
	docker push ${PROJECT}/gitlab-runner-operator:v${VERSION}

# find or download controller-gen
# download controller-gen if necessary
controller-gen:
ifeq (, $(shell which controller-gen))
	@{ \
	set -e ;\
	CONTROLLER_GEN_TMP_DIR=$$(mktemp -d) ;\
	cd $$CONTROLLER_GEN_TMP_DIR ;\
	go mod init tmp ;\
	go get sigs.k8s.io/controller-tools/cmd/controller-gen@v0.3.0 ;\
	rm -rf $$CONTROLLER_GEN_TMP_DIR ;\
	}
CONTROLLER_GEN=$(GOBIN)/controller-gen
else
CONTROLLER_GEN=$(shell which controller-gen)
endif

kustomize:
ifeq (, $(shell which kustomize))
	@{ \
	set -e ;\
	KUSTOMIZE_GEN_TMP_DIR=$$(mktemp -d) ;\
	cd $$KUSTOMIZE_GEN_TMP_DIR ;\
	go mod init tmp ;\
	go get sigs.k8s.io/kustomize/kustomize/v3@v3.5.4 ;\
	rm -rf $$KUSTOMIZE_GEN_TMP_DIR ;\
	}
KUSTOMIZE=$(GOBIN)/kustomize
else
KUSTOMIZE=$(shell which kustomize)
endif

clean-bundle:
	rm -rf bundle/manifests

# Generate bundle manifests and metadata, then validate generated files.
.PHONY: bundle
bundle: clean-bundle manifests kustomize
	operator-sdk generate kustomize manifests -q
	./scripts/create_kustomization.sh
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build config/manifests | operator-sdk generate bundle -q --overwrite --version $(VERSION) $(BUNDLE_METADATA_OPTS)
	operator-sdk bundle validate ./bundle

# Build the bundle image.
.PHONY: bundle-build
bundle-build:
	docker build -f bundle.Dockerfile -t $(BUNDLE_IMG) .

install-hack-dependencies:
	pip3 install -r hack/scripts/requirements.txt

certification-bundle: install-hack-dependencies
	$(KUSTOMIZE) build bundle | hack/scripts/parse_bundle.py

certification-bundle-build:
	docker build -f certification.Dockerfile -t gitlab-runner-operator-bundle:v${VERSION} .

certification-bundle-tag:
	docker tag gitlab-runner-operator-bundle:v${VERSION} ${PROJECT}/gitlab-runner-operator-bundle:v${VERSION}

certification-bundle-push:
	docker push ${PROJECT}/gitlab-runner-operator-bundle:v${VERSION}

tag-and-push-ubi-images:
	CERTIFIED=${CERTIFIED} ./scripts/tag_and_push_ubi_images.sh ${VERSION} ${APP_VERSION}

build-and-push-operator-image: install-hack-dependencies
	CERTIFIED=${CERTIFIED} ./scripts/build_and_push_operator_image.sh ${VERSION} ${APP_VERSION}

build-and-push-bundle-image: install-hack-dependencies
	CERTIFIED=${CERTIFIED} DEFAULT_CHANNEL=${BUNDLE_DEFAULT_CHANNEL} CHANNELS=${BUNDLE_CHANNELS} ./scripts/build_and_push_bundle_image.sh ${VERSION} ${APP_VERSION}

build-and-push-catalog-source:
	CERTIFIED=${CERTIFIED} ./scripts/build_and_push_catalog_source.sh ${VERSION} ${APP_VERSION}

operator_image_digest:
	@./scripts/operator_image_digest.sh ${VERSION}

.PHONY: generate_changelog
generate_changelog: export CHANGELOG_RELEASE ?= dev
generate_changelog: $(GITLAB_CHANGELOG)
	# Generating new changelog entries
	@$(GITLAB_CHANGELOG) -project-id 22848448 \
		-release $(CHANGELOG_RELEASE) \
		-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*" \
		-config-file .gitlab/changelog.yml \
		-changelog-file CHANGELOG.md

$(GITLAB_CHANGELOG): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(GITLAB_CHANGELOG): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(GITLAB_CHANGELOG_VERSION)/gitlab-changelog-$(OS_TYPE)-amd64"
$(GITLAB_CHANGELOG):
	# Installing $(DOWNLOAD_URL) as $(GITLAB_CHANGELOG)
	@mkdir -p $(shell dirname $(GITLAB_CHANGELOG))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(GITLAB_CHANGELOG)"
	@chmod +x "$(GITLAB_CHANGELOG)"
