# Getting Started

This brief guide aims to inform new developers what they need to get started and where all resources can be found.

## Tools
- Operator SDK
- Golang
- Make
- Kustomize
- Docker (or Podman)

## Where's everything?

```
gitlab-runner-operator $ tree -dL 1 .
.
├── api
├── bin
├── bundle
├── config
├── controllers
├── doc
├── hack
├── scripts
└── testbin
```

At the top level of the operator, you will find the following directory structure:

**api** -  contains the API definitions for that will be used to generate the Custom Resource Definitions  

**bin** - contains the generate operator binary  

**bundle** - contains the generated operator bundle used to publish the operator on Operator Hub  

**controllers** - contains the controller logic used to manage the custom resources watched by the operator  

**doc** - contains documentation about the operator  

**hack** - contains scripts, templates, release files and other miscellaneous resources needed by the project

**testbin** -  contains binaries and scripts used to create a mock Kubernetes environment against which tests can be run.  


## Running the code locally

1. You will need to install your Cluster Resource Definitions (CRDs). CRDs allow end users to extend the Kubernetes API by introducing custom resource types.

```
make install
controller-gen "crd:trivialVersions=true,preserveUnknownFields=false" rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd/bases
kustomize build config/crd | kubectl apply -f -
```

2. Edit the secret in `config/samples/runner-secret.yaml` by replacing the placeholder and create the secret:

```yaml
runner-registration-token: REPLACE_ME # your project runner secret
```

```
kubectl create -f config/samples/runner-secret.yaml
```

3. Create the Runner service account:

```
kubectl create -f bundle/manifests/gitlab-runner-sa_v1_serviceaccount.yaml
```

4. Deploy the sample GitLab Runner instance included in the `config/samples` directory:

```
kubectl create -f config/samples/runner-secret.yaml
kubectl create -f config/samples/apps_v1beta2_runner.yaml
```

5. From the operator's top level directory, run the command `make run ENABLE_WEBHOOKS=false`. The `ENABLE_WEBHOOKS=false` allows the operator to run locally with the webhooks disabled. Running with webhooks enabled requires a TLS certificate pair at `/tmp/k8s-webhook-server/serving-certs/` in your system in addition, to being able to reach the kube-apiserver through your local network.

```
make run ENABLE_WEBHOOKS=false
/home/developer/go/bin/controller-gen object:headerFile="hack/boilerplate.go.txt" paths="./..."
go fmt ./...
go vet ./...
/home/developer/go/bin/controller-gen "crd:trivialVersions=true,preserveUnknownFields=false" rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd/bases
go run ./main.go
2020-12-16T09:23:56.456-0600	INFO	environment not found	{"error": "WATCH_NAMESPACE env required"}
2020-12-16T09:23:56.943-0600	INFO	controller-runtime.metrics	metrics server is starting to listen	{"addr": ":8080"}
2020-12-16T09:23:56.944-0600	INFO	setup	starting manager
2020-12-16T09:23:56.944-0600	INFO	controller-runtime.manager	starting metrics server	{"path": "/metrics"}
2020-12-16T09:23:56.944-0600	INFO	controller	Starting EventSource	{"reconcilerGroup": "apps.gitlab.com", "reconcilerKind": "Runner", "controller": "runner", "source": "kind source: /, Kind="}
2020-12-16T09:23:57.045-0600	INFO	controller	Starting EventSource	{"reconcilerGroup": "apps.gitlab.com", "reconcilerKind": "Runner", "controller": "runner", "source": "kind source: /, Kind="}
2020-12-16T09:23:57.145-0600	INFO	controller	Starting EventSource	{"reconcilerGroup": "apps.gitlab.com", "reconcilerKind": "Runner", "controller": "runner", "source": "kind source: /, Kind="}
2020-12-16T09:23:57.245-0600	INFO	controller	Starting EventSource	{"reconcilerGroup": "apps.gitlab.com", "reconcilerKind": "Runner", "controller": "runner", "source": "kind source: /, Kind="}
2020-12-16T09:23:57.346-0600	INFO	controller	Starting Controller	{"reconcilerGroup": "apps.gitlab.com", "reconcilerKind": "Runner", "controller": "runner"}
2020-12-16T09:23:57.346-0600	INFO	controller	Starting workers	{"reconcilerGroup": "apps.gitlab.com", "reconcilerKind": "Runner", "controller": "runner", "worker count": 1}
2020-12-16T09:23:57.346-0600	INFO	controllers.Runner	Reconciling Runner	{"runner": "default/example", "name": "example", "namespace": "default"}
2020-12-16T09:23:57.659-0600	DEBUG	controller	Successfully Reconciled	{"reconcilerGroup": "apps.gitlab.com", "reconcilerKind": "Runner", "controller": "runner", "name": "example", "namespace": "default"}
```


## Building images

This section shows how to build the operator and bundle images using the makefile targets in the operator
repository.

**Note: Check out [tips-and-tricks](tips-and-tricks.md)!**

### Semi-automated

**Note: By flipping the CERTIFIED flag images will be built and tagged either for GitLab or RedHat registry. A key difference is that for non-certified images all image references are pointing to GitLab registry and RedHat is not used, while for certified images images from RedHat are used with GitLab registry as fallback**
**Note: Make sure you aren't logged with credentials permitting pushing to GitLab or RedHat registry if you don't want to push the built images**

1. Set the correct Operator and GitLab Runner version in the Makefile or pass the `VERSION` and/or `REVISION` arguments to `make`.
1. Build the Operator image:
    ```
    $ make CERTIFIED=false build-and-push-operator-image
    ```
1. Build the Operator Bundle image:
    ```
    $ make CERTIFIED=false build-and-push-bundle-image
    ```

### Automated

1. If you want to push the built images to RedHat registry automatically:
    1. Export the following environment variables in your shell by fetching them from the RedHat connect projects.
        1. `GITLAB_RUNNER_CONNECT_REGISTRY_KEY`
        1. `GITLAB_RUNNER_HELPER_CONNECT_REGISTRY_KEY`
        1. `GITLAB_RUNNER_OPERATOR_CONNECT_REGISTRY_KEY`
        1. `GITLAB_RUNNER_OPERATOR_BUNDLE_REGISTRY_KEY`
    1. If you don't want to push the images, don't set registry keys, they will just be built.
1. Run the commands from the previous sections while setting the `CERTIFIED` flag to `true`
    
