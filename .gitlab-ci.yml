image: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-operator-build-base

variables:
  GITLAB_RUNNER_OPERATOR_REGISTRY: "${CI_REGISTRY}/${CI_PROJECT_PATH}"
  GITLAB_RUNNER_OPERATOR_CI_IMAGE: "${CI_REGISTRY}/${CI_PROJECT_PATH}/ci"

stages:
  - prepare
  - test
  - release
  - release-bundle
  - release-catalog-source

.cache:
  variables:
    GOPATH: "${CI_PROJECT_DIR}/.go"
  before_script:
    - mkdir -p .go
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .go/pkg/mod/

.dind:
  services:
    - docker:dind
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_VERIFY: 1
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_CERT_PATH: "/certs/client"
  tags:
    - gitlab-org-docker

.dind-build-images:
  extends:
    - .dind
  image: $GITLAB_RUNNER_OPERATOR_CI_IMAGE

lint_code:
  stage: prepare
  script:
    - /go/bin/golint -set_exit_status $(go list ./... | grep -v /vendor/)
  extends: .cache

prepare-ci-image:
  extends:
    - .dind
  image: docker:latest
  variables:
    YQ_VERSION: "v4.4.1"
    OPERATOR_SDK_VERSION: "v1.5.0"
    OPM_VERSION: "4.6.22"
  stage: prepare
  script:
    - echo "${CI_REGISTRY_PASSWORD}" | docker login --username "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
    - docker build -f ci/Dockerfile --build-arg YQ_VERSION="${YQ_VERSION}" --build-arg OPM_VERSION="${OPM_VERSION}" --build-arg OPERATOR_SDK_VERSION="${OPERATOR_SDK_VERSION}" -t "${GITLAB_RUNNER_OPERATOR_CI_IMAGE}" .
    - docker push "${GITLAB_RUNNER_OPERATOR_CI_IMAGE}"
  only:
    changes:
      - ci/Dockerfile

unit_tests:
  stage: test
  script:
    - mkdir coverage
    - /go/bin/ginkgo -cover -outputdir=coverage ./...
  extends: .cache

.image-build:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build --pull -t "$IMAGE_TAG" --build-arg VERSION=0.0.1 .
    - docker push "$IMAGE_TAG"

.upstream-image:
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login --username "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    - 'export REVISION=`[ -z "$CI_COMMIT_TAG" ] && echo "$CI_COMMIT_SHORT_SHA" || echo`'
  after_script:
    - docker images

redhat-ubi-images:
  extends:
    - .dind-build-images
  stage: release
  script:
    - make VERSION="$CI_COMMIT_TAG" tag-and-push-ubi-images
    - docker images
  rules:
    - if: $CI_COMMIT_TAG

upstream-operator-image:
  extends:
    - .dind-build-images
    - .upstream-image
  stage: release
  script:
    - make VERSION="$CI_COMMIT_TAG" REVISION="$REVISION" CERTIFIED=false build-and-push-operator-image
    - cat hack/assets/release.yaml
  needs: ["unit_tests"]

redhat-operator-image:
  extends:
    - .dind-build-images
  stage: release
  script:
    - make VERSION="$CI_COMMIT_TAG" build-and-push-operator-image
    - cat hack/assets/release.yaml
    - docker images
  rules:
    - if: $CI_COMMIT_TAG

upstream-operator-bundle-image:
  extends:
    - .dind-build-images
    - .upstream-image
  stage: release-bundle
  script:
    - make VERSION="$CI_COMMIT_TAG" REVISION="$REVISION" CERTIFIED=false build-and-push-bundle-image
    - cat bundle/patches/related_images.yaml
  needs: ["unit_tests"]

redhat-operator-bundle-image:
  extends:
    - .dind-build-images
  stage: release-bundle
  script:
    - make VERSION="$CI_COMMIT_TAG" build-and-push-bundle-image
    - cat bundle/patches/related_images.yaml
    - docker images
  rules:
    - if: $CI_COMMIT_TAG
      when: manual

catalog-source:
  extends:
    - .dind-build-images
    - .upstream-image
  stage: release-catalog-source
  script:
    - make VERSION="$CI_COMMIT_TAG" REVISION="$REVISION" CERTIFIED=false build-and-push-catalog-source

certified-catalog-source:
  extends:
    - .dind-build-images
  stage: release-catalog-source
  script:
    - make VERSION="$CI_COMMIT_TAG" build-and-push-catalog-source
    - docker images
  rules:
    - if: $CI_COMMIT_TAG
      when: manual